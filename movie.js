const key = "c956ffa99c6cc7e6b62819ab5f833553";
const popularMovieURL = `https://api.themoviedb.org/3/movie/popular?api_key=${key}&language=en-US&page=1`;
const imagesUrl = `https://api.themoviedb.org/3/configuration?api_key=${key}`;
const searchMovieURL = `https://api.themoviedb.org/3/search/movie?api_key=${key}&query=`

var imageBase;
var imagePoster;


function isLoading() {
    $('#main').empty();
    $('#main').append(`
    <div class="d-flex justify-content-center w-100">
  <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>
      `);
}
  

async function getImageUrl(){
    const res = await fetch(imagesUrl);
    const obj = await res.json();
    imageBase = obj.images.base_url;
    imagesPoster = obj.images.poster_sizes[6];
}

async function homePage(){
    isLoading();
    getImageUrl();
    const res = await fetch(popularMovieURL);
    const movies = await res.json();
    $('#main').empty();
    for (i in movies.results){
    $('#main').append(`
      <div class="col-md-3 py-2">
          <div class="card shadow h-100">
              <img src="${imageBase}${imagesPoster}${movies.results[i].poster_path}" class="card-img-top" alt="${movies.results[i].title}">
              <div class="card-body bg-info">
                  <h5 class="card-title">${movies.results[i].title}</h5>
                  <h5>Rated: ${movies.results[i].vote_average}</h5>
                  <p>${movies.results[i].overview}</p>
                  <button class="btn btn-primary btn-sm mb-1" style="width: 150px;" type="submit" onclick=getMovieDetail(${movies.results[i].id})>Detail...</button>
              </div>
          </div>
      </div>
    `)
    }
}

async function onFormSubmit(){
    isLoading();
    const searchQuery = $('input').val();
    console.log(searchQuery);
    const res = await fetch(searchMovieURL + searchQuery);
    const movies = await res.json();
    console.log(movies);
    
    $('#main').empty();
    for (i in movies.results){
        $('#main').append(`
        <div class="col-md-3 py-2">
            <div class="card shadow h-100">
                <img src="${imageBase}${imagesPoster}${movies.results[i].poster_path}" class="card-img-top" alt="${movies.results[i].title}">
                <div class="card-body bg-info">
                    <h5 class="card-title">${movies.results[i].title}</h5>
                    <h5>Rated: ${movies.results[i].vote_average}</h5>
                    <p>${movies.results[i].overview}</p>
                    <button class="btn btn-primary btn-sm mb-1" style="width: 150px;" type="submit" onclick=getMovieDetail(${movies.results[i].id})>Detail...</button>
                </div>
            </div>
        </div>
      `)
    }
}


async function getMovieDetail(id) {
    isLoading();
    $('#main').empty();
    const creditURL = `https://api.themoviedb.org/3/movie/${id}/credits?api_key=c956ffa99c6cc7e6b62819ab5f833553`;
    fetch(creditURL)
        .then((resquest) => resquest.json())
        .then((json) => {

            let credit = json;
            console.log(credit)
            const detailURL = `https://api.themoviedb.org/3/movie/${id}?api_key=c956ffa99c6cc7e6b62819ab5f833553&language=en-US`;
            fetch(detailURL)
                .then((resqu) => resqu.json())
                .then((json) => {
                    
                    let data = json;
                    rev_url = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=c956ffa99c6cc7e6b62819ab5f833553&language=en-US&page=1`
                    fetch(rev_url)
                        .then((resp) => resp.json())
                        .then(json => {
                            let reviews = json.results;
                            $('#main').empty();
                            $('#main').append(`
                    <div class="row no-gutters bg-seconadary position-relative shadow p-1 bg-secondary text-white rounded">
                    <div class="col-md-6 mb-md-0 p-md-4">
                        <img src="https://image.tmdb.org/t/p/w780/${data.poster_path}" class=" shadow bg-seconadary rounded style="max-width: 29rem;"" alt="${data.title}" height="680px" width="440px">
                    </div>
                    <div class="col-md-6 position-static p-4 pl-md-0">
                        <h1 class=" " role="alert">
                            ${data.title}
                        </h1>
                        <hr/>
                        <p>Genre: ${data.genres[0].name}, ${data.genres[1].name}</p>
                        <p>Overview: ${data.overview}</p>
                        <p>Actors: <a class="text-primary link" onclick=getActorDetail_test(${credit.cast[0].id})>${credit.cast[0].name}</a>, <a class="text-primary link" onclick=getActorDetail_test(${credit.cast[1].id})>${credit.cast[1].name}</a>, <a class="text-primary link" onclick=getActorDetail_test(${credit.cast[2].id})>${credit.cast[2].name}</a>, <a class="text-primary link" onclick=getActorDetail_test(${credit.cast[3].id})>${credit.cast[3].name}</a>...</p>
                        <p>Crews: ${credit.crew[0].name}, ${credit.crew[1].name}, ${credit.crew[2].name}...</p>
                        <p>Original language: ${data.original_language}</p>
                        <p>Spoken languages: ${data.spoken_languages[0].name}</p>
                        <p>Length: ${data.runtime} minutes</p>
                        <p>Released date: ${data.release_date}</p>
                        <p>Production countries: ${data.production_countries[0].name}</p>
                        
                        </div>
                        <p>
                        <button class="btn btn-primary ml-4" type="button" data-toggle="collapse" style="width: 200px;" data-target="#reviewCollapse" aria-expanded="false"   aria-controls="collapseExample">Show Review</button>
                        </p>
                        <div class="collapse" id="reviewCollapse">
                        <div class="card card-body ml-4 mr-4 mb-4 text-dark">
                        <p>Authors: ${reviews[0].author}</p>
                        <hr/>
                        <p>${reviews[0].content}</p>
                        </div>
                        </div>
                </div>
            `)

                        })
                })
        })
        .catch(function(error) {
            $('#main').empty();
            alert("There are no movies matched your search")
            console.log(error);
        });
}

async function eventSubmitByActor(event) {
    event.preventDefault();
    var value;

    $("input")
        .keyup(function() {
            value = $(this).val();
        })
        .keyup();
    isLoading();
    const creditURL = `https://api.themoviedb.org/3/search/person?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${value}&page=1&include_adult=false`;

    fetch(creditURL)
        .then((request) => request.json())
        .then(function(data) {
            $('#main').empty();
            let actors = data.results;
            console.log(actors)

            for (const actor of actors) {
                $('#main').append(`
                
                <div class="col-md-3 py-2">
            <div class="card shadow h-100">
                <img src="https://image.tmdb.org/t/p/w780/${actor.known_for[0].poster_path}" class="card-img-top" alt="${actor.known_for[0].title}}">
                <div class="card-body bg-info">
                    <h5 class="card-title">${actor.known_for[0].title}</h5>
                    <h5>Rated: ${actor.known_for[0].vote_average}</h5>
                    <p>${actor.known_for[0].overview}</p>
                    <button class="btn btn-primary btn-sm mb-1" style="width: 150px;" type="submit" onclick=getMovieDetail(${actor.known_for[0].id})>Detail...</button>
                </div>
            </div>
        </div>`);
                
            }
        })
        .catch(function(error) {
            $('#main').empty();
            alert("There are no movies matched your search")
            console.log(error);
        });
}